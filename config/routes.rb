Rails.application.routes.draw do

  root   'categories#index'

  get    '/login', to: 'sessions#new'
  post   '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get     '/signup', to: 'users#new'
  post    '/signup', to: 'users#create'
  get     '/users',  to: 'users#index'
  get     '/user',   to: 'users#show'
  resources :users

  resources :goods, only: [:index, :show]
  get     '/search/goods', to: 'goods#search'

  resources :categories do
    resources :goods
  end

  get       '/shopping_cart',                to:  'shopping_cart#show'
  get       '/shopping_cart/total_sum',      to:  'shopping_cart#total_sum'
  post      '/shopping_cart/goods/:good_id', to:  'shopping_cart#add'
  delete    '/shopping_cart/goods/:good_id', to:  'shopping_cart#remove'

  resources :orders do
    resources :goods
    resources :credits
  end

  get       '/credits',                        to: 'credits#index'
  post      '/credits/:id',                    to: 'credits#create'

  resources :banks

  get       '/payments',    to: 'payments#index'
  post      '/payment',     to: 'payments#new'

  get       '/admin/dashboard',   to: 'admin#index'
  namespace :admin do
    resources :users, only: [:index, :change_role]
    post      '/users/:user_id/role',to: 'users#change_role'

    get       '/orders',            to: 'orders#index'
    post      '/orders/:order_id',  to: 'orders#confirm'
    delete    '/orders/:order_id',  to: 'orders#cancel'

    resources :categories, only: [:index, :create]
    put       '/categories/:category_id',     to: 'categories#update'
    delete    '/categories/:category_id',     to: 'categories#delete'

    resources :goods, only: [:index, :create]
    put       '/goods/:good_id',     to: 'goods#update'
    delete    '/goods/:good_id',     to: 'goods#delete'
    get       '/search/goods',       to: 'goods#search'

    resources :credits, only: [:create]

    resources :orders, only: [:index]
    post      '/orders/:order_id/credit',     to: 'credits#create'
    delete    '/orders/:order_id',            to: 'orders#cancel'


  end

end
