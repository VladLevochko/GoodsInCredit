--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE banks (
    id integer NOT NULL,
    name character varying,
    interest_rate double precision,
    fine double precision,
    payment_term integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: banks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banks_id_seq OWNED BY banks.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp without time zone
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: credits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE credits (
    id integer NOT NULL,
    loan_term integer,
    rest double precision,
    monthly_payment double precision,
    fine double precision,
    bank_id integer,
    order_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: credits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE credits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: credits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE credits_id_seq OWNED BY credits.id;


--
-- Name: goods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE goods (
    id integer NOT NULL,
    name text,
    price integer,
    description text,
    number integer,
    number_of_reserved integer,
    category_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    photo_file_name character varying,
    photo_content_type character varying,
    photo_file_size integer,
    photo_updated_at timestamp without time zone,
    search_vector tsvector
);


--
-- Name: goods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE goods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: goods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE goods_id_seq OWNED BY goods.id;


--
-- Name: goods_orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE goods_orders (
    good_id integer,
    order_id integer
);


--
-- Name: goods_shopping_carts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE goods_shopping_carts (
    good_id integer,
    shopping_cart_id integer
);


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE orders (
    id integer NOT NULL,
    total_price double precision,
    status character varying,
    months integer,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    price double precision
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE payments (
    id integer NOT NULL,
    amount double precision,
    credit_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payments_id_seq OWNED BY payments.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: shopping_carts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE shopping_carts (
    id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: shopping_carts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE shopping_carts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shopping_carts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE shopping_carts_id_seq OWNED BY shopping_carts.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    full_name character varying,
    id_number integer,
    password_digest character varying,
    email character varying,
    phone_number character varying,
    role character varying DEFAULT 'guest'::character varying,
    remember_digest character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: banks id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banks ALTER COLUMN id SET DEFAULT nextval('banks_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: credits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY credits ALTER COLUMN id SET DEFAULT nextval('credits_id_seq'::regclass);


--
-- Name: goods id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY goods ALTER COLUMN id SET DEFAULT nextval('goods_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- Name: payments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments ALTER COLUMN id SET DEFAULT nextval('payments_id_seq'::regclass);


--
-- Name: shopping_carts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY shopping_carts ALTER COLUMN id SET DEFAULT nextval('shopping_carts_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: banks banks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY banks
    ADD CONSTRAINT banks_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: credits credits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY credits
    ADD CONSTRAINT credits_pkey PRIMARY KEY (id);


--
-- Name: goods goods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goods
    ADD CONSTRAINT goods_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: shopping_carts shopping_carts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY shopping_carts
    ADD CONSTRAINT shopping_carts_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_credits_on_bank_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_credits_on_bank_id ON credits USING btree (bank_id);


--
-- Name: index_credits_on_order_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_credits_on_order_id ON credits USING btree (order_id);


--
-- Name: index_goods_on_category_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goods_on_category_id ON goods USING btree (category_id);


--
-- Name: index_goods_orders_on_good_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goods_orders_on_good_id ON goods_orders USING btree (good_id);


--
-- Name: index_goods_orders_on_order_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goods_orders_on_order_id ON goods_orders USING btree (order_id);


--
-- Name: index_goods_shopping_carts_on_good_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goods_shopping_carts_on_good_id ON goods_shopping_carts USING btree (good_id);


--
-- Name: index_goods_shopping_carts_on_shopping_cart_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_goods_shopping_carts_on_shopping_cart_id ON goods_shopping_carts USING btree (shopping_cart_id);


--
-- Name: index_orders_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_user_id ON orders USING btree (user_id);


--
-- Name: index_payments_on_credit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payments_on_credit_id ON payments USING btree (credit_id);


--
-- Name: index_shopping_carts_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_shopping_carts_on_user_id ON shopping_carts USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: search_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX search_index ON goods USING gin (search_vector);


--
-- Name: goods goods_search_vectore_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER goods_search_vectore_update BEFORE INSERT OR UPDATE ON goods FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('search_vector', 'pg_catalog.english', 'name', 'description');


--
-- Name: goods_shopping_carts fk_rails_343bb6e046; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goods_shopping_carts
    ADD CONSTRAINT fk_rails_343bb6e046 FOREIGN KEY (shopping_cart_id) REFERENCES shopping_carts(id);


--
-- Name: credits fk_rails_523966fa2c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY credits
    ADD CONSTRAINT fk_rails_523966fa2c FOREIGN KEY (bank_id) REFERENCES banks(id);


--
-- Name: goods_shopping_carts fk_rails_58750c0e0f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goods_shopping_carts
    ADD CONSTRAINT fk_rails_58750c0e0f FOREIGN KEY (good_id) REFERENCES goods(id);


--
-- Name: goods fk_rails_5b7fc611ab; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goods
    ADD CONSTRAINT fk_rails_5b7fc611ab FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- Name: shopping_carts fk_rails_8228391db2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY shopping_carts
    ADD CONSTRAINT fk_rails_8228391db2 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: credits fk_rails_8892dfb1c3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY credits
    ADD CONSTRAINT fk_rails_8892dfb1c3 FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- Name: payments fk_rails_9e4765d5d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT fk_rails_9e4765d5d2 FOREIGN KEY (credit_id) REFERENCES credits(id);


--
-- Name: goods_orders fk_rails_b85d752459; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goods_orders
    ADD CONSTRAINT fk_rails_b85d752459 FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- Name: goods_orders fk_rails_eec4783b60; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY goods_orders
    ADD CONSTRAINT fk_rails_eec4783b60 FOREIGN KEY (good_id) REFERENCES goods(id);


--
-- Name: orders fk_rails_f868b47f6a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT fk_rails_f868b47f6a FOREIGN KEY (user_id) REFERENCES users(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('1'), ('10'), ('2'), ('20161206092739'), ('20161206122446'), ('20161207193713'), ('20161216181105'), ('20161220004702'), ('3'), ('4'), ('5'), ('6'), ('7'), ('8'), ('9');


