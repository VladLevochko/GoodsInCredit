# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

PATH = 'app/assets/images/good_img.png'

admin = User.new(full_name: 'General Admin', email: 'admin@gic.com', password: '123456', role: :admin, id_number: 1234567890, phone_number: '0991234567')
bill = User.new(full_name: 'bill', email: 'b@gm.cm', password: '123456', role: :user, id_number: 1234567890, phone_number: '0991234567')
stiv = User.new(full_name: 'stiv', email: 's@gm.cm', password: '123456', role: :user, id_number: 1234567890, phone_number: '0991234567')

sc_a = ShoppingCart.new
sc_b = ShoppingCart.new
sc_s = ShoppingCart.new

admin.shopping_cart = sc_a
bill.shopping_cart = sc_b
stiv.shopping_cart = sc_s

admin.save
bill.save
stiv.save

phones = Category.new(name: 'Phones')
phones.image_from_url(PATH)
phones.save
computers = Category.new(name: 'Computers')
computers.image_from_url(PATH)
computers.save
tvs = Category.new(name: 'TVs')
tvs.image_from_url(PATH)
tvs.save
has = Category.new(name: 'Household appliances')
has.image_from_url(PATH)
has.save

(1..100).each { |i|
  g=Good.new(name: "Phone #{i}", price: (100 * i + i) % 107, number: 100, number_of_reserved: 0, category: phones, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris leo arcu, accumsan a mauris in, fringilla placerat leo. In faucibus ex a sodales aliquet. Aenean in ornare erat. Pellentesque malesuada posuere egestas. Pellentesque neque orci, pulvinar vitae sapien sed, faucibus ultricies augue. Aliquam cursus est arcu, id efficitur ligula vulputate eget. Duis elit lorem, venenatis molestie nisl ac, fermentum lacinia nunc. Quisque ipsum mauris, maximus ac consectetur at, pellentesque non est. Pellentesque nunc sapien, eleifend et sagittis vitae, egestas vel leo. Nam elementum egestas nisl. Mauris nec metus a metus suscipit vulputate.')
  g.photo_from_url(PATH)
  g.save
}

(1..100).each { |i|
  g=Good.new(name: "Computer #{i}", price: (1000 * i + i) % 1007, number: 1000, number_of_reserved: 0, category: computers, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris leo arcu, accumsan a mauris in, fringilla placerat leo. In faucibus ex a sodales aliquet. Aenean in ornare erat. Pellentesque malesuada posuere egestas. Pellentesque neque orci, pulvinar vitae sapien sed, faucibus ultricies augue. Aliquam cursus est arcu, id efficitur ligula vulputate eget. Duis elit lorem, venenatis molestie nisl ac, fermentum lacinia nunc. Quisque ipsum mauris, maximus ac consectetur at, pellentesque non est. Pellentesque nunc sapien, eleifend et sagittis vitae, egestas vel leo. Nam elementum egestas nisl. Mauris nec metus a metus suscipit vulputate.')
  g.photo_from_url(PATH)
  g.save
}

(1..100).each { |i|
  g=Good.new(name: "TV #{i}", price: (10000 * i + i) % 100007, number: 10000, number_of_reserved: 0, category: tvs, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris leo arcu, accumsan a mauris in, fringilla placerat leo. In faucibus ex a sodales aliquet. Aenean in ornare erat. Pellentesque malesuada posuere egestas. Pellentesque neque orci, pulvinar vitae sapien sed, faucibus ultricies augue. Aliquam cursus est arcu, id efficitur ligula vulputate eget. Duis elit lorem, venenatis molestie nisl ac, fermentum lacinia nunc. Quisque ipsum mauris, maximus ac consectetur at, pellentesque non est. Pellentesque nunc sapien, eleifend et sagittis vitae, egestas vel leo. Nam elementum egestas nisl. Mauris nec metus a metus suscipit vulputate.')
  g.photo_from_url(PATH)
  g.save
}

(1..100).each { |i|
  g=Good.new(name: "Household appliance #{i}", price: (1000 * i + i) % 1007, number: 1000, number_of_reserved: 0, category: has, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris leo arcu, accumsan a mauris in, fringilla placerat leo. In faucibus ex a sodales aliquet. Aenean in ornare erat. Pellentesque malesuada posuere egestas. Pellentesque neque orci, pulvinar vitae sapien sed, faucibus ultricies augue. Aliquam cursus est arcu, id efficitur ligula vulputate eget. Duis elit lorem, venenatis molestie nisl ac, fermentum lacinia nunc. Quisque ipsum mauris, maximus ac consectetur at, pellentesque non est. Pellentesque nunc sapien, eleifend et sagittis vitae, egestas vel leo. Nam elementum egestas nisl. Mauris nec metus a metus suscipit vulputate.')
  g.photo_from_url(PATH)
  g.save
}

Bank.create(name: 'World Wide Bank', interest_rate: 10, fine: 10, payment_term: 7)