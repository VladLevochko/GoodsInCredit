class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :full_name
      t.integer :id_number
      t.string :password_digest
      t.string :email
      t.string :phone_number
      t.string :role, :default => :guest
      t.string :remember_digest

      t.timestamps
    end
  end

  # before_validation(on: :create) do
  #   self.phone_number = phone_number.gsub(/[^0-9]/, "") if attribute_present?("phone_number")
  # end
end
