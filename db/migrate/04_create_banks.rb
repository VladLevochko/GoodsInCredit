class CreateBanks < ActiveRecord::Migration[5.0]
  def change
    create_table :banks do |t|
      t.string :name
      t.float :interest_rate
      t.float :fine
      t.integer :payment_term

      t.timestamps
    end
  end
end
