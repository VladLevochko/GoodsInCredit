class GoodsOrdersJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_table :goods_orders, id: false do |t|
      t.belongs_to :good, index: true, foreign_key: true
      t.belongs_to :order, index: true, foreign_key: true
    end
  end
end
