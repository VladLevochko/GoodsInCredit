class CreateCredits < ActiveRecord::Migration[5.0]
  def change
    create_table :credits do |t|
      t.integer :loan_term
      t.float :rest
      t.float :monthly_payment
      t.float :fine
      t.belongs_to :bank, index: true, foreign_key: true
      t.belongs_to :order, index: true, foreign_key: true

      t.timestamps
    end
  end
end
