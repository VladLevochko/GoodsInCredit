class CreateGoods < ActiveRecord::Migration[5.0]
  def change
    create_table :goods do |t|
      t.text :name
      t.integer :price
      t.text :description
      t.integer :number
      t.integer :number_of_reserved

      t.belongs_to :category, index: true, foreign_key: true

      t.timestamps
    end

  end
end
