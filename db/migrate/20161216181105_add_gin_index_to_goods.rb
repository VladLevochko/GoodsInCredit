class AddGinIndexToGoods < ActiveRecord::Migration[5.0]
  def up
    #adding search vector column
    add_column :goods, :search_vector, 'tsvector'

    #creating gin index
    execute <<-SQL
      CREATE INDEX search_index
      ON goods
      USING GIN(search_vector);
    SQL

    #creating trigger for updating and creating search_vector
    execute <<-sql
      DROP TRIGGER IF EXISTS goods_search_vector_update ON goods;

      CREATE TRIGGER goods_search_vectore_update 
      BEFORE INSERT OR UPDATE 
      ON goods
      FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger(search_vector, 'pg_catalog.english', name, description);
    sql

    Good.find_each {|g| g.touch}
  end

  def down
    remove_column :goods, :search_vector
    execute <<-sql
      DROP INDEX search_index;
      DROP TRIGGER IF EXISTS goods_search_vector_update ON goods;
    sql
  end
end
