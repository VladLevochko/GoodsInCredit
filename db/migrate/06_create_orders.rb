class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.float :total_price
      t.string :status
      t.integer :months
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps
    end
  end
end
