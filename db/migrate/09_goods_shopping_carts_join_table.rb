class GoodsShoppingCartsJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_table :goods_shopping_carts, id: false do |t|
      t.belongs_to :good, index: true, foreign_key: true
      t.belongs_to :shopping_cart, index: true, foreign_key: true
    end
  end
end
