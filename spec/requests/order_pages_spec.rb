require 'rails_helper'

describe 'OrderPages' do
  subject { page }
  let(:user) { FactoryGirl.create(:user) }

  let(:submit) { 'Orders' }

  before { visit orders_path(user) }
  it { should have_content('Private Office')}

  it "should have the content 'Orders'" do
    visit orders_path(user)
    # expect(page).to have_title('Orders')
  end

  # it "should redirect to 'Orders'" do
  #   expect { click_button submit }.to redirect_to('/orders')
  # end
end
