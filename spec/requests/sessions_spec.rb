require 'rails_helper'

describe 'Sessions', :type => :feature do
  describe 'Home page' do

    it "should have the content 'Log in'" do
      visit '/login'
      expect(page).to have_content('Log in')
      click_button 'Log in'
      # expect(page).to have_content '/login'
      # expect(page).to have_current_path(root_path)
    end
  end
end
