require 'rails_helper'

describe 'CategoryPages' do
  subject { page }

  describe 'Categories' do
    before { visit categories_path }
    let(:category) { FactoryGirl.create(:category) }

    it { should have_css('div.row') }
    # it { should have_content(category.name)}
  end
end
