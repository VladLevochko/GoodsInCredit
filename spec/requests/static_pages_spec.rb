require 'rails_helper'

describe 'StaticPages' do
  describe 'Home page' do

    it "should have the content 'Goods in Credit'" do
      visit root_path
      expect(page).to have_content('Goods in Credit')
    end

    it "should have the title 'Home'" do
      visit root_path
      expect(page).to have_title('Goods in Credit')
    end
  end

  describe 'Private Office' do

    it "should have the content 'Log in'" do
      visit login_path
      expect(page).to have_content('Log in')
    end

    it "should have the content 'Sign up'" do
      visit signup_path
      expect(page).to have_content('Sign up')
    end

    it "should have the content 'Profile settings'" do
      visit '/user'
      expect(page).to have_content('Office')
    end

    it "should have the content 'Orders'" do
      visit orders_path
    end

    it "should have the content 'Credits'" do
      visit credits_path
    end

    it "should have the content 'Payments'" do
      visit payments_path
    end

    it "should have the content 'Log out'" do
      visit root_path
    end
  end

  # describe "Help page" do
  #
  #   it "should have the content 'Help'" do
  #     visit '/static_pages/help'
  #     expect(page).to have_content('Help')
  #   end
  #
  #   it "should have the title 'Help'" do
  #     visit '/static_pages/help'
  #     expect(page).to have_title("Ruby on Rails Tutorial Sample App | Help")
  #   end
  # end
  #
  # describe "About page" do
  #
  #   it "should have the content 'About Us'" do
  #     visit '/static_pages/about'
  #     expect(page).to have_content('About Us')
  #   end
  #
  #   it "should have the title 'About Us'" do
  #     visit '/static_pages/about'
  #     expect(page).to have_title("Ruby on Rails Tutorial Sample App | About Us")
  #   end
  # end
end
