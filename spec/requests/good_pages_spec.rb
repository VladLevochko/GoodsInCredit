require 'rails_helper'

describe 'GoodPages' do
  subject { page }
  let(:category) { FactoryGirl.create(:category) }

  describe 'Goods' do
    before { visit category_goods_path(category) }

    it { should have_content('Goods') }
    # it { should have_content('Add to cart') }
  end
end
