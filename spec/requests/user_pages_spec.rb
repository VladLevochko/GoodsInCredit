require 'rails_helper'

describe 'UserPages' do
  subject { page }

  let(:submit) { 'Sign up' }

  describe 'Signup page' do

    before { visit signup_path }

    it { should have_content('Sign up') }

    describe 'with invalid information' do
      it 'should not create a user' do
        expect { click_button submit }.not_to change(User, :count)
      end

      describe 'after submission' do
        before { click_button submit }

        it { should have_content('Sign up') }
        it { should have_content('error') }
      end
    end

    describe 'with valid information' do
      before do
        fill_in 'Name', with: 'Example User'
        fill_in 'VAT identification number', with: '12345678'
        fill_in 'Email address', with: 'user@example.com'
        fill_in 'Password', with: '1a2b3c'
        fill_in 'Confirmation', with: '1a2b3c'
        fill_in 'Phone', with: '0931276183'
        # click_button 'Submit'
      end

      it 'should create a user' do
        expect { click_button submit }.to change{User.count}.by(1)
      end

      # describe "after saving the user" do
      #   before { click_button submit }
      #   let(:user) { User.find_by(email: 'user@example.com') }
      #
      #   it { should have_title(user.name) }
      #   it { should have_selector('div.alert.alert-success') }
      # end
    end
  end

  describe 'Profile page' do
    let(:user) { FactoryGirl.create(:user) }
    before { visit user_path(user) }

    it {should have_content('Office') }
  end

  # describe 'edit' do
  #   let(:user) { FactoryGirl.create(:user) }
  #   before do
  #     # logged_in user
  #     visit edit_user_path(user)
  #   end
  #
  #   describe 'with valid information' do
  #     let(:new_full_name)  { 'New Name' }
  #     let(:new_email) { 'new@example.com' }
  #     before do
  #       fill_in 'Name', with: new_full_name
  #       fill_in 'VAT identification number', with: user.id_number
  #       fill_in 'Email address', with: new_email
  #       fill_in 'Password', with: user.password
  #       fill_in 'Confirmation', with: user.password
  #       fill_in 'Phone', with: user.phone_number
  #       # click_button 'Submit'
  #     end
  #
  #     # it { should have_selector('div.alert.alert-success') }
  #     # it { should have_link('Sign out', href: signout_path) }
  #     specify { expect(user.reload.full_name).to  eq new_full_name }
  #     specify { expect(user.reload.email).to eq new_email }
  #   end
  #
  # end
end
