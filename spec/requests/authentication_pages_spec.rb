require 'rails_helper'

describe 'AuthenticationPages' do
  subject { page }

  let(:submit) { 'Log in' }

  describe 'Login page' do
    before { visit login_path }

    it { should have_content('Log in') }
    it { should have_content('Remember me') }

    describe 'Lognin' do
      before { visit login_path }

      # describe 'with invalid information' do
      #   before { click_button 'Sign in' }
      #
      #   it { should have_content('Sign in') }
      #   it { should have_selector('div.alert.alert-danger') }
      #
      #   describe 'after visiting another page' do
      #     before { click_link 'Home' }
      #     it { should_not have_selector('div.alert.alert-danger') }
      #   end
      # end

      describe 'with valid information' do
        let(:user) { FactoryGirl.create(:user) }
        before do
          fill_in 'Email',    with: user.email.upcase
          fill_in 'Password', with: user.password
          click_button 'Log in'
        end

        it { should have_link('Profile settings',     href: edit_user_path(user)) }
        it { should have_link('Log out',    href: logout_path) }
        it { should_not have_link('Orders', href: orders_path(user)) }
        it { should_not have_link('Credits', href: credits_path(user)) }
        it { should_not have_link('Payments', href: payments_path(user)) }

        describe 'followed by logout' do
          before { click_link('Log out') }
          it { should have_link('Log in') }
        end
      end

      # describe 'after saving the user' do
      #   before { click_button submit }
      #   let(:user) { User.find_by(email: 'user@example.com') }
      #
      #   it { should have_link('Log out') }
      #   it { should have_selector('div.btn.btn-success.btn-lg') }
      # end
    end
  end

  describe 'authorization' do

    describe 'for non-logged-in users' do
      let(:user) { FactoryGirl.create(:user) }

      describe 'in the Users controller' do

        describe 'visiting the edit page' do
          before { visit edit_user_path(user) }
          it { should have_content('Log in') }
        end
        #
        # describe 'submitting to the update action' do
        #   before { post signup_path }
        #   specify { expect(response).to redirect_to(login_path) }
        # end
      end
    end

    # describe 'as wrong user' do
    #   let(:user) { FactoryGirl.create(:user) }
    #   let(:wrong_user) { FactoryGirl.create(:user, email: 'wrong@example.com') }
    #   before { sign_in user, no_capybara: true }
    #
    #   describe 'submitting a GET request to the Users#edit action' do
    #     before { get edit_user_path(wrong_user) }
    #     specify { expect(response.body).not_to match(content('Office')) }
    #     specify { expect(response).to redirect_to(root_url) }
    #   end
    #   #
    #   # describe 'submitting a PATCH request to the Users#update action' do
    #   #   before { patch user_path(wrong_user) }
    #   #   specify { expect(response).to redirect_to(root_url) }
    #   # end
    # end
  end
end
