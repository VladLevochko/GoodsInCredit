require 'rails_helper'

describe User do
  let(:user) { FactoryGirl.create(:user) }
  let(:order) { FactoryGirl.create(:order) }

  it { should have_many(:orders) }

  it {should respond_to(:full_name)}
  it {should respond_to(:id_number)}
  it {should respond_to(:email)}
  it {should respond_to(:password)}
  it {should respond_to(:password_confirmation)}
  it {should respond_to(:phone_number)}
  it {should respond_to(:role)}

  it { should respond_to(:password_confirmation) }
  it { should respond_to(:remember_token) }
  it { should respond_to(:authenticate) }

  it 'is valid' do
    expect(user).to be_valid
  end

  it 'validates full_name' do
    expect(user).to be_valid(:full_name)
  end

  describe 'when full_name is not present' do
    before { user.full_name = ' ' }
    it {should_not be_valid}
  end

  describe 'when full_name is too long' do
    before { user.full_name = 'a' * 256 }
    it { should_not be_valid }
  end

  describe 'when id_number is not present' do
    before { user.id_number = ' ' }
    it { should_not be_valid }
  end

  describe 'when id_number is too short' do
    before { user.id_number = 'a' * 7 }
    it { should_not be_valid }
  end

  describe 'when email is not present' do
    before { user.email = ' ' }
    it { should_not be_valid }
  end

  describe 'when email is too long' do
    before { user.email = 'a' * 256 }
    it { should_not be_valid }
  end

  describe 'when email format is invalid' do
    it 'should be invalid' do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        user.email = invalid_address
        expect(user).not_to be_valid
      end
    end
  end

  describe 'when email format is valid' do
    it 'should be valid' do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        user.email = valid_address
        expect(user).to be_valid
      end
    end
  end

  describe 'when email address is already taken' do
    before do
      user_with_same_email = user.dup
      user_with_same_email.email = user.email.upcase
      user_with_same_email.save
    end

    it { should_not be_valid }
  end

  describe 'email address with mixed case' do
    let(:mixed_case_email) { 'Foo@ExAMPle.CoM' }

    it 'should be saved as all lower-case' do
      user.email = mixed_case_email
      user.save
      expect(user.reload.email).to eq mixed_case_email.downcase
    end
  end

  describe 'when password is not present' do
    before { user.password = user.password_confirmation = ' ' }
    it { should_not be_valid }
  end

  describe 'with a password that is too short' do
    before { user.password = user.password_confirmation = 'a' * 5 }
    it { should be_invalid }
  end

  describe 'when password does not match confirmation' do
    before { user.password_confirmation = 'mismatch' }
    it { should_not be_valid }
  end

  it { should respond_to(:authenticate) }

  describe 'return value of authenticate method' do
    before { user.save }
    let(:found_user) { User.find_by(email: user.email) }

    it 'with valid password' do
      expect(user).to eq found_user.authenticate(user.password)
    end

    describe 'with invalid password' do
      let(:user_for_invalid_password) { found_user.authenticate('invalid') }

      it { should_not eq user_for_invalid_password }
      specify { expect(user_for_invalid_password).to be false }
    end
  end

  describe 'when phone_number is not present' do
    before { user.phone_number = ' ' }
    it { should_not be_valid }
  end

  describe 'when phone_number is too short' do
    before { user.phone_number = 'a' * 9 }
    it { should be_invalid }
  end

  describe 'when phone_number format is valid' do
    it 'should be valid' do
      user.phone_number = '0931276183'
      if user.phone_number == /0[569][035-9](\d){7}/
        expect(user).to be_valid
      end
    end

    it 'should be invalid' do
      user.phone_number = 'imvalid_number'
      unless ['pending', 'confirmed', 'cancelled', 'paid'].include?(user.phone_number)
        expect(user).to_not be_valid
      end
    end
  end

  describe 'when role is not present' do
    before { user.role = ' ' }
    it { should_not be_valid }
  end

  describe 'when role format is valid' do
    it 'should be valid' do
      roles = ['guest', 'user', 'admin', 'manager']
      roles.each do |valid_role|
        user.role = valid_role
        expect(user).to be_valid
      end
    end

    it 'should be invalid' do
      user.role = 'invalid_role'
      unless ['pending', 'confirmed', 'cancelled', 'paid'].include?(user.role)
        expect(user).to_not be_valid
      end
    end
  end

  describe 'orders associations' do

    let(:user_with_order) { create :user_with_order}

    it 'should destroy associated orders' do
      user.destroy
      expect(user.orders).to be_empty
      # expect(user_with_order).to be_empty
    end
  end

  describe 'remember token' do
    before { user.save }
    it(:remember_token) { should_not be_blank }
  end
end