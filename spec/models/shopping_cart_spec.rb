require 'rails_helper'

describe ShoppingCart do
  let(:shopping_cart) { FactoryGirl.create(:shopping_cart) }

  it {should respond_to(:user)}
  it {should respond_to(:user_id)}
  it {should have_and_belong_to_many :goods}

  it 'is valid' do
    expect(shopping_cart).to be_valid
  end

  describe 'when user_id is not present' do
    before { shopping_cart.user_id = nil }
    it { should_not be_valid }
  end

  describe 'good association' do
    let(:good) { create :good }
    let(:shopping_cart) { create :shopping_cart }
    let(:goods_shopping_carts) { create :goods_shopping_carts, good:good, shopping_cart: shopping_cart }
  end
end
