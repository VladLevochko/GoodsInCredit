require 'rails_helper'

describe Payment do
  let(:credit) { FactoryGirl.create(:credit) }
  let(:payment) { FactoryGirl.create(:payment) }

  it { should respond_to(:amount) }
  it { should respond_to(:credit) }
  it { should respond_to(:credit_id) }

  it 'is valid' do
    expect(payment).to be_valid
  end

  describe 'amount is not present' do
    before { payment.amount = ' ' }
    it {should_not be_valid}
  end
end
