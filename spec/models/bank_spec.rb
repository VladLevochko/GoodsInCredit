require 'rails_helper'

describe Bank do
  let(:bank) { FactoryGirl.create(:bank) }
  let(:credit) { FactoryGirl.create(:credit)}

  it { should have_many(:credits) }

  it {should respond_to(:name)}
  it {should respond_to(:interest_rate)}
  it {should respond_to(:fine)}
  it {should respond_to(:payment_term)}

  it 'is valid' do
    expect(bank).to be_valid
  end

  it 'validates name' do
    expect(bank).to be_valid(:name)
  end

  describe 'when name is not present' do
    before { bank.name = ' ' }
    it {should_not be_valid}
  end

  describe 'when name is too long' do
    before { bank.name = 'a' * 151 }
    it { should_not be_valid }
  end

  describe 'when interest_rate is not present' do
    before { bank.interest_rate = ' ' }
    it { should_not be_valid }
  end

  describe 'when fine is not present' do
    before { bank.fine = ' ' }
    it { should_not be_valid }
  end

  describe 'when payment_term is not present' do
    before { bank.payment_term = ' ' }
    it { should_not be_valid }
  end

  describe 'credits associations' do

    let(:bank_with_credit) { create :bank_with_credit}

    it 'should destroy associated credits' do
      bank.destroy
      expect(bank.credits).to be_empty
      # expect(bank_with_credit).not_to exist
    end
  end
end
