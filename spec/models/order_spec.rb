require 'rails_helper'

describe Order do
  let(:user) { FactoryGirl.create(:user)}
  let(:order) { FactoryGirl.create(:order, user: user) }

  it {should respond_to(:total_price)}
  it {should respond_to(:status)}
  it {should respond_to(:months)}
  it {should respond_to(:user)}
  it {should respond_to(:user_id)}

  # it {should has_one(:credit)}

  it 'is valid' do
    expect(order).to be_valid
  end

  it 'validates total price' do
    expect(order).to be_valid(:total_price)
  end

  describe 'when total price is not present' do
    before { order.total_price = ' ' }
    it { should_not be_valid }
  end

  describe 'when status is not present' do
    before { order.status = ' ' }
    it { should_not be_valid }
  end

  describe 'when status format' do
    it 'should be valid' do
      statuses = ['pending', 'confirmed', 'cancelled', 'paid']
      statuses.each do |valid_status|
        order.status = valid_status
        expect(order).to be_valid
      end
    end

    it 'should be invalid' do
      order.status = 'invalid_status'
      unless ['pending', 'confirmed', 'cancelled', 'paid'].include?(order.status)
        expect(order).to_not be_valid
      end
    end
  end

  describe 'when number of months is not present' do
    before { order.months = ' ' }
    it { should_not be_valid }
  end

  describe 'when user_id is not present' do
    before { order.user_id = nil }
    it { should_not be_valid }
  end
end

