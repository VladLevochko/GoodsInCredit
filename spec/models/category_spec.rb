require 'rails_helper'

describe Category do
  let(:category) { FactoryGirl.create(:category) }

  it { should have_many(:goods) }

  it {should respond_to(:name)}
  it {should respond_to(:image)}
  it { should respond_to(:goods) }

  it { should have_attached_file(:image) }

  it 'is valid' do
    expect(category).to be_valid
  end

  it 'validates name' do
    expect(category).to be_valid(:name)
  end

  describe 'when name is not present' do
    before { category.name = ' ' }
    it {should_not be_valid}
  end

  describe 'when name is too long' do
    before { category.name = 'a' * 101 }
    it { should_not be_valid }
  end

  describe 'when image is not present' do
    before { image = nil }
    it {should_not be_valid}
  end

  describe 'image content type validation' do
    it { should validate_attachment_content_type(:image).allowing('image/png', 'image/jpg', 'image/jpeg').
        rejecting('text/plain', 'text/html', 'text/xml', 'application/octet-stream', 'application/exe') }
  end

  describe 'image size validation' do
    it { should validate_attachment_size(:image).
        in(0..10.kilobytes)}
  end

  describe 'goods associations' do
    before { category.save }
    let!(:goods) {FactoryGirl.create(:good, category: category)}

    it 'should destroy associated goods' do
      goods = category.goods.to_a
      category.destroy
      expect(goods).not_to be_empty
      goods.each do |good|
        expect(Good.where(id: good.id)).to be_empty
      end
    end
  end
end
