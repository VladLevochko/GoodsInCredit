require 'rails_helper'

describe Good do
  let(:category) { FactoryGirl.create(:category) }
  let(:good) { FactoryGirl.create(:good, category: category) }

  it { should belong_to(:category) }
  it { should have_and_belong_to_many :shopping_carts }
  it { should have_and_belong_to_many :orders }

  it {should respond_to(:name)}
  it {should respond_to(:price)}
  it {should respond_to(:description)}
  it {should respond_to(:number)}
  it {should respond_to(:number_of_reserved)}
  it {should respond_to(:photo)}
  it {should respond_to(:category)}
  it {should respond_to(:category_id)}

  it {should have_attached_file(:photo)}

  it 'is valid' do
    expect(good).to be_valid
  end

  it 'validates name' do
    expect(good).to be_valid(:name)
  end

  # it 'is associated with at least one shopping cart' do
  #   # FactoryGirl.create(:organization)
  #   it { should have_and_belong_to_many(:shopping_carts)}
  # end

  describe 'when name is not present' do
    before { good.name = ' ' }
    it { should_not be_valid }
  end

  describe 'when name is too long' do
    before { good.name = 'a' * 101 }
    it { should_not be_valid }
  end

  describe 'when price is not present' do
    before { good.price = ' ' }
    it { should_not be_valid }
  end

  describe 'when price is less then 0' do
    before { good.price = '0'}
    it {should_not be_valid }
  end

  describe 'when description is not present' do
    before { good.description = ' ' }
    it { should_not be_valid }
  end

  describe  'when description is too short' do
    before { good.description = 'a' * 9 }
    it { should_not be_valid }
  end

  describe 'when number is not present' do
    before { good.number = ' ' }
    it { should_not be_valid }
  end

  describe 'when photo is not present' do
    before { photo = nil }
    it {should_not be_valid}
  end

  describe 'photo content type validation' do
    it { should validate_attachment_content_type(:photo).allowing('image/png', 'image/jpg', 'image/jpeg').
        rejecting('text/plain', 'text/html', 'text/xml', 'application/octet-stream', 'application/exe') }
  end

  describe 'photo size validation' do
    it { should validate_attachment_size(:photo).
        in(0..10.kilobytes)}
  end

  describe 'when category_id is not present' do
    before { good.category_id = nil }
    it { should_not be_valid }
  end

  describe 'shopping cart association' do
    let(:shopping_cart) { create :shopping_cart }
    let(:good) { create :good }
    let(:goods_shopping_carts) { create :goods_shopping_carts, shopping_cart: shopping_cart, good: good }

    it 'should delete all associated goods' do
      shopping_cart.destroy
      expect(shopping_cart.goods).to be_empty
    end
  end

  describe 'order association' do
    let(:order) { create :order }
    let(:good) { create :good }
    let(:goods_orders) { create :goods_orders, order: order, good: good }

    it 'should delete all associated goods' do
      order.destroy
      expect(order.goods).to be_empty
    end
  end
end
