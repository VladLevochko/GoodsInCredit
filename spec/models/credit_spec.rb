require 'rails_helper'

describe Credit do
  let(:bank) { FactoryGirl.create(:bank) }
  let(:credit) { FactoryGirl.create(:credit) }
  let(:payment) { FactoryGirl.create(:payment)}

  it { should have_many(:payments) }

  it {should respond_to(:loan_term)}
  it {should respond_to(:rest)}
  it {should respond_to(:monthly_payment)}
  it {should respond_to(:fine)}
  it {should respond_to(:bank)}
  it {should respond_to(:bank_id)}
  it {should respond_to(:order)}
  it {should respond_to(:order_id)}

  it 'is valid' do
    expect(credit).to be_valid
  end

  describe 'when loan_term is not present' do
    before { credit.loan_term = ' ' }
    it {should_not be_valid}
  end

  describe 'when rest is not present' do
    before { credit.rest = ' ' }
    it {should_not be_valid}
  end

  describe 'when monthly_payment is not present' do
    before { credit.monthly_payment = ' ' }
    it {should_not be_valid}
  end

  describe 'when fine is not present' do
    before { credit.fine = ' ' }
    it {should_not be_valid}
  end

  describe 'when bank_id is not present' do
    before { credit.bank_id = nil }
    it { should_not be_valid }
  end

  describe 'when order_id is not present' do
    before { credit.order_id = nil }
    it { should_not be_valid }
  end

  describe 'payments associations' do

    let(:credit_with_payment) { create :credit_with_payment}

    it 'should destroy associated credits' do
      credit.destroy
      expect(credit.payments).to be_empty
      # expect(bank_with_credit).not_to exist
    end
  end
end
