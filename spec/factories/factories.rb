FactoryGirl.define do
  factory :user do
    full_name 'Example User'
    id_number '12345678'
    email 'user@example.com'
    password '1a2b3c'
    # password_confirmation '1a2b3c'
    phone_number '0931276183'
    role 'user'

    factory :user_with_order do
      after_create do |user|
        create(:order, user: user)
      end
    end
  end

  factory :category do
    name 'Computers'
    image { File.new('spec/fixtures/ex_img.png') }
  end

  factory :good do
    name 'Computer'
    price '100'
    description 'this is a computer'
    number '100'
    number_of_reserved '5'
    photo { File.new('spec/fixtures/ex_img.png') }
    category_id '1'
    association :category
  end

  factory :shopping_cart do
    user_id '1'
    association :user
  end

  factory :order do
    total_price '3000'
    status 'pending'
    months '10'
    user_id '1'
    association :user
  end

  factory :bank do
    name 'Privet bank'
    interest_rate '10'
    fine '10'
    payment_term '10'

    factory :bank_with_credit do
      after_create do |bank|
        create(:credit, bank: bank)
      end
    end
  end

  factory :credit do
    loan_term '10'
    rest '20000'
    monthly_payment '500'
    fine '100'
    bank_id '1'
    association :bank
    order_id '1'
    association :order

    factory :credit_with_payment do
      after_create do |credit|
        create(:payment, credit: credit)
      end
    end
  end

  factory :payment do
    amount '1000'
    credit_id '1'
    association :credit
  end

  factory :goods_orders do
    good
    order
  end

  factory :goods_shopping_carts do
    good
    shopping_cart
  end
end