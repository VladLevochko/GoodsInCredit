class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include BanksHelper
  include CreditsHelper
  include ShoppingCartHelper

  def index

  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

end
