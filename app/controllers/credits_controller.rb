class CreditsController < ApplicationController
  load_and_authorize_resource

  def index
    @orders = current_user.orders.where(status: [:confirmed, :paid])
  end

end
