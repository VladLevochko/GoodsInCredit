class GoodsController < ApplicationController
  load_and_authorize_resource

  def index
    if params[:order].nil?
      @goods = Category.find(params[:category_id]).goods.paginate(page: params[:page], per_page: 20)
    else
      order = params[:order] == 'desc' ? 'DESC' : 'ASC'
      @goods = Category.find(params[:category_id]).goods.order("price #{order}").paginate(page: params[:page], per_page: 20)
    end
  end


  def show
    @good = Good.find(params[:id])
  end

  def add_to_shopping_cart
    @current_user.shopping_cart << Good.find(params[:id])
  end

  def search
    @goods = Good.search(params[:text]).paginate(page: params[:page], per_page: 20)
  end

  private
    def good_params
      params.require(:good).permit(:name, :price, :number, :photo)
    end
end
