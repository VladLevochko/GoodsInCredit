class BanksController < ApplicationController

  def index
    @banks = Bank.all
  end

  def new
    @band = Bank.new
  end

  def create
    @bank = Bank.new(bank_params)
    if @bank.save
      redirect_to banks_path
    else
      render new
    end
  end

  def update

  end

  private
    def bank_params
      params.require(:bank).expect(:name, :interest_rate, :fine)
    end
end
