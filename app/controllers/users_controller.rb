class UsersController < ApplicationController
  load_and_authorize_resource

  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]

  def index
  end

  def show
    @user = current_user
  end

  #render page with sign up form
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.role = :user
    if @user.save
      log_in @user
      flash[:info] = %Q[Welcome in GoodsInCredit!
                        Now you can buy something in
                        <a href='#{root_path}'>GoodsInCredit Catalog</a>].html_safe
      redirect_to edit_user_path(current_user)
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to show
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    #flash[:success] = 'User deleted'
    redirect_to users_path
  end

  private
    def user_params
      params.require(:user).permit(:full_name, :id_number, :email,
                                   :password, :password_confirmation,
                                   :phone_number)
    end

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        #flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(categories_path) unless current_user?(@user)
    end
end
