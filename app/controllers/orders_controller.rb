class OrdersController < ApplicationController
  load_and_authorize_resource

  def index
    @orders = current_user.orders
  end

  def show
    @order = Order.find(params[:id])
  end

  def create
    @order = Order.new
    @order.goods = current_user.shopping_cart.goods
    total_price = 0
    @order.goods.each do |good|
      if good.number > 0
          good.number -= 1
          total_price += good.price
      else
        flash[:error] = "Sorry, but there are no #{good.name} in our storehouse..."
        redirect_to shopping_cart_path
        return
      end

    end
    @order.status = 'pending'
    @order.months = params[:months].to_i
    @order.price = total_price
    @order.total_price = Credit.credit_total(@order.price, @order.months, current_bank.interest_rate  / (12 * 100))

    current_user.orders << @order

    if @order.save
      current_user.shopping_cart.goods.delete_all
      current_user.shopping_cart = ShoppingCart.new
      redirect_to orders_path
    else
      flash[:error] = 'Order can\'t be created for some reasons'
      redirect_to shopping_cart_path
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.delete
  end
end
