class ShoppingCartController < ApplicationController
  load_and_authorize_resource

  def show
    @shopping_cart = current_user.shopping_cart
  end

  #add good to shopping cart of current user
  def add
    good = Good.find(params[:good_id])
    unless current_user.shopping_cart.goods.exists?(good)
      current_user.shopping_cart.goods << good
    end
    redirect_to shopping_cart_path
  end

  def remove
    good = Good.find(params[:good_id])
    current_user.shopping_cart.goods.delete(good)
    redirect_to shopping_cart_path
  end

  def total_sum
    l = current_user.shopping_cart.total_sum
    n = params[:months].to_i
    i = current_bank.interest_rate / (12 * 100)
    credit_total_sum = Credit.credit_total(l, n, i)

    respond_to do |format|
      format.json { render json: {'credit_total_sum': credit_total_sum.round(2) }}
    end
  end

end
