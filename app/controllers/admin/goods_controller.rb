class Admin::GoodsController < AdminController

  def index
    if !params[:text].nil?
      @goods = Good.search(params[:text]).paginate(page: params[:page], per_page: 30)
    elsif !params[:good].nil? && !params[:good][:category_id].nil?
      @goods = Good.where(category_id: params[:good][:category_id]).paginate(page: params[:page], per_page: 30)
    else
      @goods = Good.all.paginate(page: params[:page], per_page: 30)
    end

    @good = Good.new
    @categories = Category.all
  end

  def create
    @good = Good.new(good_params)
    @good.number_of_reserved = 0
    if @good.save
      redirect_to admin_goods_path
    else
      flash[:error] = 'Error adding good!'
      redirect_to admin_goods_path
    end
  end

  def update
    @good = Good.find(params[:good_id])
    @good.update_attributes(good_params)

    redirect_to admin_goods_path
  end

  def delete
    Good.delete(params[:good_id])

    redirect_to admin_goods_path
  end

  private
    def good_params
      params.require(:good).permit(:name, :price, :photo, :description, :category_id, :number)
    end

end