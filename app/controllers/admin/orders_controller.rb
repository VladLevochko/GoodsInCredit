class Admin::OrdersController < AdminController

  def index
    @orders = Order.where(status: :pending)
  end

  def cancel
    @order = Order.find(params[:order_id])
    @order.update(status: :cancelled)

    redirect_to index
  end
end