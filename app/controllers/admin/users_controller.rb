class Admin::UsersController < AdminController

  def index
    @users = User.all
  end

  def change_role
    @user = User.find(params[:user_id])
    if @user.update_attribute(:role, params[:role])
      render json: {
          status: 200,
          message: 'Role successfully changed!'
      }.to_json
    else
      render json: {
          status: @user.error.status,
          message: @user.error.message
      }.to_json
    end
  end
end