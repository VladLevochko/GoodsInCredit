class Admin::CreditsController < AdminController

  def create
    @order = Order.find(params[:order_id])
    credit = Credit.new
    credit.loan_term = @order.months
    credit.monthly_payment = @order.total_price.to_d / @order.months.to_d
    credit.bank = current_bank
    credit.rest = @order.total_price
    credit.fine = 0
    @order.update(status: :confirmed, credit: credit)

    redirect_to admin_orders_path
  end

end