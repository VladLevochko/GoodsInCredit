class Admin::CategoriesController < AdminController

  def index
    @category = Category.new
    @categories = Category.all
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_categories_path
    else
      flash[:error] = 'error adding category!'
      redirect_to admin_categories_path
    end

  end

  def update
    @category = Category.find(params[:category_id])
    @category.update_attributes(category_params)

    redirect_to admin_categories_path
  end

  def delete
    Category.delete(params[:category_id])

    redirect_to admin_categories_path
  end

  private
    def category_params
      params.require(:category).permit(:name, :image)
    end
end