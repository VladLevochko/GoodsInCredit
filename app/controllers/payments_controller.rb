class PaymentsController < ApplicationController
  load_and_authorize_resource

  def index
    @payment = Payment.new
    @orders = current_user.orders.where(status: :confirmed)
  end

  def new
    @payment = Payment.new(payment_params)
    @credit = Credit.find(@payment.credit_id)
    if @credit.order.is?('confirmed')
      if @payment.amount.to_f >= @credit.minimal_payment.to_f
        @credit.add_payment(@payment)
        flash[:success] = "Payment for credit #{@credit.id} successfully made!"
      else
        flash[:error] = 'Amount is less than minimal payment for this credit!'
      end
    else
      flash[:error] = "Payment not available for that credit! Credit status #{@credit.order.status}"
    end

    redirect_to payments_path
  end

  private
    def payment_params
      params.require(:payment).permit(:credit_id, :amount)
    end

end
