/**
 * Created by vlad on 17.12.16.
 */

$(document).ready(function() {

    $("select.role_select").on("change", function(){
        var user_id = this.id;
        var role = this.value;
        $.ajax({
            url: '/admin/users/' + user_id + '/role',
            method: 'POST',
            data: {
                user_id: user_id,
                role: role
            },
            dataType: 'json',
            success: function(data) {
                console.log(data)
            },
            error: function(data) {
                console.log(data)
            }
        })
    })
});