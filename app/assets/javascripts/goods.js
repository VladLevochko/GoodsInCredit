/**
 * Created by vlad on 17.12.16.
 */

$(document).ready(function(){
    $('#asc_order_button').on('click', function() {
        sendOrderRequest('asc')
    });

    $('#desc_order_button').on('click', function() {
        sendOrderRequest('desc')
    });
});

function sendOrderRequest(order) {
    $.ajax({
        url: $(location).attr('href'),
        type: 'get',
        data: { order: order },
        dataType: 'json'
    });
}