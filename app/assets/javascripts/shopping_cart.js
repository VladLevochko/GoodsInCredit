/**
 * Created by vlad on 03.12.16.
 */

var slider;
var number;

$(document).ready(function() {
    slider = $('#sliderInput');
    number = $('#numberInput');

    setTotal(2);

    number.on('change', function(){
        slider.slider('setValue', number.val());
        setTotal(number.val());
    });

    slider.slider().on('slideStop', function() {
        number.val(parseInt(slider.val()));
        setTotal(slider.val());
    })
});

function setTotal(months) {
    $.ajax({
        url: '/shopping_cart/total_sum',
        method: 'GET',
        data: { months: months },
        dataType: "json",
        success: function(data) {
            console.log(data);
            $("#total_sum").text(data.credit_total_sum);
            $("#monthly").text((parseInt(data.credit_total_sum) / parseInt(months)).toFixed(2))
        },
        error: function(data) {
            console.log(data);
            console.log('error');
        }
    });

}

function calculateTotal() {
    var months = $("#numberInput").val();
    var percent = 5;
    var goods_total = 0;
    $('.good_price').each(function() {
        goods_total += parseInt($( this ).text());
    });

    // return goods_total * months * percent + goods_total;
    return goods_total + goods_total * (Math.pow(1 + percent / 100, months) - 1)
}

//toggle for pop-up window
function show(state){

    document.getElementById('window').style.display = state;
    document.getElementById('wrap').style.display = state;
}



// function monthsAmountSlider() {
//     var sliderInput = $("#sliderInput");
//     var numberInput = $("#numberInput");
//     if (sliderInput.val < 2 || sliderInput.val == "")
//         sliderInput.val = 2;
//     numberInput.val = sliderInput.val;
// }
//
// function monthsAmountNumber() {
//     var sliderInput = $("#sliderInput");
//     var numberInput = $("#numberInput");
//
//     if (numberInput.val < 2 || numberInput.val == "")
//         numberInput.val(2);
//     sliderInput.val(numberInput.val);
// }
