class User < ApplicationRecord
  #constants
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_PHONE_NUMBER = /0[569][035-9](\d){7}/
  ROLES = [:guest, :user, :admin, :manager]

  #relations
  has_many :orders#, dependent: :destroy
  has_one :shopping_cart, dependent: :destroy

  #accessors
  attr_accessor :remember_token

  #callback
  before_destroy { orders.destroy_all }
  before_save {
    self.email = email.downcase
    self.shopping_cart = ShoppingCart.new
  }

  #constraints
  validates :full_name, presence: true, length: { maximum: 255 }
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  validates :id_number, presence: true, length: { minimum: 8, maximum: 10 }
  validates :phone_number, presence: true, format: { with: VALID_PHONE_NUMBER }
  has_secure_password
  validates :password, presence: true, confirmation:  true, length: { minimum: 6 }
  validates :role, presence: true, inclusion: { in: ['guest', 'user', 'admin', 'manager'] }

  #class methods
  class << self

    # Returns the hash digest of the given string.
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
          BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end

    # Returns a random token.
    def new_token
      SecureRandom.urlsafe_base64
    end

  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  def is?(requested_role)
    self.role == requested_role.to_s
  end

end
