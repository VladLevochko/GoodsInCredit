class ShoppingCart < ApplicationRecord
  has_and_belongs_to_many :goods
  belongs_to :user

  validates :user_id, presence: true

  before_destroy {goods.clear}

  def total_sum
    sum = 0
    self.goods.each do |good|
      sum += good.price
    end
    sum
  end
end
