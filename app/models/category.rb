class Category < ApplicationRecord
  has_many :goods, dependent: :delete_all

  has_attached_file :image

  def image_from_url(url)
    self.image = open(url)
  end

  validates :name, presence: true, length: { maximum: 100 }
  validates_attachment :image,
                       presence: true,
                       size: { in: 0..512.kilobytes },
                       content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png'] }
end
