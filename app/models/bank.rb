class Bank < ApplicationRecord
  has_many :credits#, dependent: :delete_all
  #belongs_to :credit
  before_destroy {credits.destroy_all}

  validates :name, presence: true, length: {maximum: 150}
  validates :interest_rate, presence: true
  validates :fine, presence: true
  validates :payment_term, presence: true
end
