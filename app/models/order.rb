class Order < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :goods
  has_one :credit, dependent: :destroy

  STATUSES = [:pending, :confirmed, :cancelled, :paid]

  before_destroy {goods.clear}

  validates :total_price, presence: true
  validates :status, presence: true, inclusion: { in: ['pending', 'confirmed', 'cancelled', 'paid'] }
  validates :months, presence: true
  validates :user_id, presence: true

  def is?(requested_status)
    self.status == requested_status
  end
end
