class Payment < ApplicationRecord
  belongs_to :credit
  #has_one :credit

  validates :amount, presence: true
end
