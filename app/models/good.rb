class Good < ApplicationRecord
  belongs_to :category
  has_and_belongs_to_many :shopping_carts
  has_and_belongs_to_many :orders

  before_destroy {shopping_carts.clear}
  before_destroy {orders.clear}

  has_attached_file :photo, :as => :owner

  def photo_from_url(url)
    self.photo = open(url)
  end

  validates :name, presence: true, length: { maximum: 100 }
  validates :price, presence: true, :numericality => {:greater_than => 0}
  validates :description, presence: true, length:  {minimum: 10 }
  validates :number, presence: true
  validates :number_of_reserved, presence: true
  validates :category_id, presence: true
  validates_attachment :photo,
                       presence: true,
                       size: { in: 0..512.kilobytes },
                       content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png'] }

  def self.search(text = '')
    sanitized = sanitize_sql_array(["to_tsquery('english', ?)", text.gsub(/\s/,"+")])
    Good.where("search_vector @@ #{sanitized}")
  end
end
