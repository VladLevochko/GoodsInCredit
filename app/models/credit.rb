class Credit < ApplicationRecord
  include BanksHelper

  belongs_to :order
  belongs_to :bank
  has_many :payments, dependent: :delete_all

  before_destroy {payments.destroy_all}

  validates :loan_term, presence: true
  validates :rest, presence: true
  validates :monthly_payment, presence: true
  validates :fine, presence: true
  validates :order_id, presence: true

  def add_payment(payment)
    self.payments << payment
    self.rest -= payment.amount

    if self.rest <= 0
      self.order.update(status: :paid)
    end

    self.save!
  end

  def self.credit_total(sum, term, interest)
    sum + (2 * sum - sum * (term - 1) / term) / 2 * term * interest
  end

  def minimal_payment
    check_fine

    amount = self.order.total_price
    term = self.order.months
    payment_number = self.payments.count
    month_rate = current_bank.interest_rate / (12 * 100)

    [(amount / term) + (amount - (amount / term) * payment_number) * month_rate + self.fine,
     self.rest + self.fine].min.round(2)
  end

  def check_fine
    if self.payments.empty?
      last_payment = self.created_at
    else
      last_payment = self.payments.order(:created_at).last.created_at
    end
    if Date.today.yday - last_payment.yday > 31
      self.fine = (Date.today.yday - last_payment.yday - 31) * self.rest * 0.01
    end
  end

  def next_payment_date
    if self.payments.empty?
      self.created_at + 1.month
    else
      self.payments.order(:created_at).last.created_at + 1.month
    end
  end

end
