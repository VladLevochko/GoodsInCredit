module CreditsHelper

  def credit_sum(term, sum)
    sum + (2 * sum - (sum * (term - 1) / term)) / 2 * term * current_bank.interest_rate / 100
  end

end
