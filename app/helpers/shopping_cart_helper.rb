module ShoppingCartHelper

  def shopping_cart
    if current_user
      @shopping_cart = current_user.shopping_cart
    else
      ShoppingCart.new
    end
  end

  def goods_in_cart
    @current_user.shopping_cart.goods.count
  end
end
